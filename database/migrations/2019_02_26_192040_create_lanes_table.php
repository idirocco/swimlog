<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('lanes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('stroke_id');
            $table->enum('distance', [25, 50, 100, 200, 400, 800])->default(50);
            $table->time('time', 2)->nullable();
            $table->string('laps')->nullable();
            $table->enum('status', ['not started', 'in progress', 'completed'])->default('not started');
            $table->unsignedInteger('heat_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('stroke_id')->references('id')->on('strokes');
            $table->foreign('heat_id')->references('id')->on('heats');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('lanes');
    }
}
