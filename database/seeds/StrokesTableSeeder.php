<?php

use Illuminate\Database\Seeder;
use App\Stroke;

class StrokesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Stroke::truncate();

        Stroke::create([
            'name' => 'Freestyle',
            'abbr' => 'FR',
        ]);

        Stroke::create([
            'name' => 'Butterfly',
            'abbr' => 'FL',
        ]);

        Stroke::create([
            'name' => 'Backstroke',
            'abbr' => 'BK',
        ]);

        Stroke::create([
            'name' => 'Breaststroke',
            'abbr' => 'BR',
        ]);

        Stroke::create([
            'name' => 'Individual Medley',
            'abbr' => 'IM',
        ]);
    }
}
