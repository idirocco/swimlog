<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $faker = \Faker\Factory::create();

        Category::create([
            'name' => 'Infantil 1',
            'ageFrom' => 10,
            'ageTo' => 10,
        ]);

        Category::create([
            'name' => 'Infantil 2',
            'ageFrom' => 11,
            'ageTo' => 11,
        ]);

        Category::create([
            'name' => 'Menor 1',
            'ageFrom' => 12,
            'ageTo' => 12,
        ]);

        Category::create([
            'name' => 'Menor 2',
            'ageFrom' => 13,
            'ageTo' => 13,
        ]);

        Category::create([
            'name' => 'Cadete',
            'ageFrom' => 14,
            'ageTo' => 15,
        ]);

        Category::create([
            'name' => 'Juvenil',
            'ageFrom' => 16,
            'ageTo' => 18,
        ]);

        Category::create([
            'name' => 'Mayor',
            'ageFrom' => 19,
            'ageTo' => 23,
        ]);
    }
}
