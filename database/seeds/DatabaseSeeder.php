<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(TeamsTableSeeder::class);
        $this->call(StrokesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MeetsTableSeeder::class);
        $this->call(SessionsTableSeeder::class);
        $this->call(EventStatusesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(HeatsTableSeeder::class);
        $this->call(LanesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
