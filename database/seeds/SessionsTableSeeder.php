<?php

use Illuminate\Database\Seeder;
use App\Session;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Session::truncate();

        $faker = \Faker\Factory::create();

        Session::create([
            'name' => 'Sesión 1',
            'starts_at' => '2019-01-09 14:00:00',
            'meet_id' => 1,
        ]);

        Session::create([
            'name' => 'Sesión 2',
            'starts_at' => '2019-01-10 08:00:00',
            'meet_id' => 1,
        ]);
    }
}
