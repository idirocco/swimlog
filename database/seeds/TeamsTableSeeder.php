<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Team::truncate();

        $faker = \Faker\Factory::create();

        Team::create([
            'name' => 'Union y Progeso',
            'location' => 'Tandil, Buenos Aires, Argentina'
        ]);
    }
}
