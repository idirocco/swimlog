<?php

use Illuminate\Database\Seeder;
use App\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Event::truncate();

        $faker = \Faker\Factory::create();

        Event::create([
            'name' => 'Evento 1 - Hombres 50 Libre',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);

        Event::create([
            'name' => 'Evento 2 - Mujeres 50 Libre',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);

        Event::create([
            'name' => 'Evento 3 - Hombres 50 Pecho',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);

        Event::create([
            'name' => 'Evento 4 - Mujeres 50 Pecho',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);

        Event::create([
            'name' => 'Evento 5 - Hombres 100 CI',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);

        Event::create([
            'name' => 'Evento 6 - Mujeres 100 CI',
            'date' => '2019-01-09 14:00:00',
            'event_status_id' => 1,
            'session_id' => 1,
        ]);
        ///////////////////////////////
        Event::create([
            'name' => 'Evento 7 - Hombres 100 Libre',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);

        Event::create([
            'name' => 'Evento 8 - Mujeres 100 Libre',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);

        Event::create([
            'name' => 'Evento 9 - Hombres 50 Espalda',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);

        Event::create([
            'name' => 'Evento 10 - Mujeres 50 Espalda',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);

        Event::create([
            'name' => 'Evento 11 - Hombres 200 CI',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);

        Event::create([
            'name' => 'Evento 12 - Mujeres 200 CI',
            'date' => '2019-01-10 14:00:00',
            'event_status_id' => 1,
            'session_id' => 2,
        ]);
    }
}
