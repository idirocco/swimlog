<?php

use Illuminate\Database\Seeder;
use App\Lane;

class LanesTableSeeder extends Seeder
{
    /**
     * Lane the database seeds.
     */
    public function run()
    {
        // Let's clear the users table first
        Lane::truncate();

        $faker = \Faker\Factory::create();

        $now = new DateTime();
        $times = ['0:2:01.51', '0:1:00.28', '0:1:04.53', '0:1:03.41', '0:3:02.16', '0:0:27.33', '0:0:53.27', '0:0:33.85', '0:2:52.90', '0:0:29.15'];

        for ($i = 0; $i < 30; ++$i) {
            Lane::create([
                'user_id' => rand(1, 20),
                'stroke_id' => rand(1, 5),
                'time' => $times[rand(0, 9)],
                'status' => 'completed',
                'heat_id' => rand(1, 3),
            ]);
        }
    }
}
