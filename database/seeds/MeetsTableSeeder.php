<?php

use Illuminate\Database\Seeder;
use App\Meet;

class MeetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Meet::truncate();

        $faker = \Faker\Factory::create();

        Meet::create([
            'name' => 'Torneo SFyUP',
            'location' => 'Tandil, Buenos Aires, Argentina',
            'dateFrom' => '2019-02-09',
            'dateTo' => '2019-02-10',
        ]);
    }
}
