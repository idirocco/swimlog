<?php

use Illuminate\Database\Seeder;
use App\EventStatus;

class EventStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        EventStatus::truncate();

        $faker = \Faker\Factory::create();

        EventStatus::create([
            'name' => 'Not started',
        ]);
        EventStatus::create([
            'name' => 'In progress',
        ]);
        EventStatus::create([
            'name' => 'Completed',
        ]);
    }
}
