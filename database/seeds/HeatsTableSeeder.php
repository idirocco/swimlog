<?php

use Illuminate\Database\Seeder;
use App\Heat;

class HeatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Heat::truncate();

        $faker = \Faker\Factory::create();

        Heat::create([
            'name' => 'Heat 1',
            'date' => '2019-01-09 14:00:00',
            'event_id' => rand(1, 10),
        ]);

        Heat::create([
            'name' => 'Heat 2',
            'date' => '2019-01-09 14:00:00',
            'event_id' => rand(1, 10),
        ]);
        Heat::create([
            'name' => 'Heat 3',
            'date' => '2019-01-09 14:00:00',
            'event_id' => rand(1, 10),
        ]);
    }
}
