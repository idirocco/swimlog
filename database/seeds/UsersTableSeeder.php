<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Let's clear the users table first
        User::truncate();

        $faker = \Faker\Factory::create();

        // Let's make sure everyone has the same password and
        // let's hash it before the loop, or else our seeder
        // will be too slow.
        $password = Hash::make('nacho');

        $names = ['Mati Nu', 'Pauli Gappo', 'JuanPi Bona', 'Fede Go', 'Marian Beta', 'Caro Pizza', 'Joako Cincu', 'Hernan Agudo', 'Mati Ben', 'Eva Baranda', 'Diego Ga', 'Fer Dallas', 'Romi Tarr', 'Santi Pena', 'Victor Doy', 'Ine Rus', 'Facu', 'Juli', 'Sofi', 'Mauro Fiesta', 'Chiche Gelblun', 'El Quique', 'Juani Bachi'];

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 21; ++$i) {
            User::create([
                'name' => $names[$i],
                'email' => $faker->email,
                'password' => $password,
                'phone' => $faker->PhoneNumber,
                'category_id' => rand(1, 7),
                'gender' => 0 == $i % 2 ? 'male' : 'female',
                'team_id' => 1,
            ]);
        }
    }
}
