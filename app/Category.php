<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Get the sessions for the meet.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
