<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meet extends Model
{
    protected $fillable = ['name', 'date'];

    /**
     * Get the sessions for the meet.
     */
    public function sessions()
    {
        return $this->hasMany('App\Session');
    }
}
