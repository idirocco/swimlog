<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lane extends Model
{
    protected $fillable = ['user_id', 'date', 'stroke_id', 'heat_id', 'time', 'laps', 'distance', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function stroke()
    {
        return $this->belongsTo('App\Stroke');
    }

    public function heat()
    {
        return $this->belongsTo('App\Heat');
    }
}
