<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heat extends Model
{
    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function session()
    {
        return $this->belongsTo('App\Session');
    }

    /**
     * Get the lanes of the heat.
     */
    public function lanes()
    {
        return $this->hasMany('App\Lane');
    }
}
