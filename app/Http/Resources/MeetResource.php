<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'dateFrom' => $this->dateFrom,
            'dateTo' => $this->dateTo,
            'location' => $this->location,
            'sessions' => SessionResource::collection($this->sessions),
        ];
    }
}
