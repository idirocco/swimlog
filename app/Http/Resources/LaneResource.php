<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LaneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user_name' => $this->user->name,
            'date' => $this->updated_at,
            'distance' => $this->distance,
            'heat_id' => $this->heat_id,
            'stroke' => new StrokeResource($this->stroke),
            'status' => $this->status,
            'time' => $this->time,
        ];
    }
}
