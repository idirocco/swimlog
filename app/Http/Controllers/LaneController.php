<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lane;
use App\Http\Resources\LaneResource;

class LaneController extends Controller
{
    public function index()
    {
        return LaneResource::collection(Lane::all());
    }

    public function show(Lane $lane)
    {
        return new LaneResource($lane);
    }

    public function store(Request $request)
    {
        $lane = Lane::create($request->json()->all());

        $bestTime = Lane::where([
            'user_id' => $lane->user_id,
            'stroke_id' => $lane->stroke_id,
            'distance' => $lane->distance,
            'status' => 'completed',
        ])->orderBy('time', 'asc')
        ->pluck('time')
        ->first();

        $lane = $lane->toArray();
        $lane['best_time'] = $bestTime ? substr($bestTime, 3) : 'N/T';

        return response()->json($lane, 201);
    }

    public function update(Request $request, Lane $lane)
    {
        $lane->update($request->all());

        return response()->json($lane, 200);
    }

    public function delete(Lane $lane)
    {
        $lane->delete();

        return response()->json(null, 204);
    }
}
