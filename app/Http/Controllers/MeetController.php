<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meet;
use App\Http\Resources\MeetResource;

class MeetController extends Controller
{
    public function index()
    {
        return MeetResource::collection(Meet::all());
    }

    public function show(Meet $meet)
    {
        return new MeetResource($meet);
    }

    public function store(Request $request)
    {
        $meet = Meet::create($request->all());

        return response()->json($meet, 201);
    }

    public function update(Request $request, Meet $meet)
    {
        $meet->update($request->all());

        return response()->json($meet, 200);
    }

    public function delete(Meet $meet)
    {
        $meet->delete();

        return response()->json(null, 204);
    }
}
