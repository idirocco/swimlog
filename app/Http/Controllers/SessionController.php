<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\Http\Resources\Session as SessionResource;

class SessionController extends Controller
{
    public function index()
    {
        return SessionResource::collection(Session::all());
    }

    public function listByMeet($meet_id)
    {
        return SessionResource::collection(Session::all()->where('meet_id', $meet_id));
    }

    public function show(Session $session)
    {
        return Session::find($session);
    }

    public function store(Request $request)
    {
        $session = Session::create($request->all());

        return response()->json($session, 201);
    }

    public function update(Request $request, Session $session)
    {
        $session->update($request->all());

        return response()->json($session, 200);
    }

    public function delete(Session $session)
    {
        $session->delete();

        return response()->json(null, 204);
    }
}
