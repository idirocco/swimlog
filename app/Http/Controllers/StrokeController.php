<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stroke;
use App\Http\Resources\StrokeResource;

class StrokeController extends Controller
{
    public function index()
    {
        return StrokeResource::collection(Stroke::all());
    }

    public function show(Stroke $Stroke)
    {
        return new StrokeResource($Stroke);
    }

    public function store(Request $request)
    {
        $Stroke = Stroke::create($request->all());

        return response()->json($Stroke, 201);
    }

    public function update(Request $request, Stroke $Stroke)
    {
        $Stroke->update($request->all());

        return response()->json($Stroke, 200);
    }

    public function delete(Stroke $Stroke)
    {
        $Stroke->delete();

        return response()->json(null, 204);
    }
}
