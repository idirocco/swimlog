<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Heat;
use App\Http\Resources\HeatResource;

class HeatController extends Controller
{
    public function index()
    {
        return HeatResource::collection(Heat::all());
    }

    public function show(Heat $heat)
    {
        return new HeatResource($heat);
    }

    public function store(Request $request)
    {
        $heat = Heat::create($request->all());

        return response()->json($heat, 201);
    }

    public function update(Request $request, Heat $heat)
    {
        $heat->update($request->all());

        return response()->json($heat, 200);
    }

    public function delete(Heat $heat)
    {
        $heat->delete();

        return response()->json(null, 204);
    }
}
