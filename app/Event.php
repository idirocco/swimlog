<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function heats()
    {
        return $this->hasMany('App\Heat');
    }
}
