<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * Get the meet that owns the session.
     */
    public function meet()
    {
        return $this->belongsTo('App\Meet');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
