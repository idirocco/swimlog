<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'cors'], function () {
    Route::resource('meet', 'MeetController');

    Route::resource('heat', 'HeatController');

    Route::resource('lane', 'LaneController');

    Route::resource('category', 'CategoryController');

    Route::resource('session', 'SessionController');
    Route::get('/meet/{meet_id}/sessions', 'SessionController@listByMeet');

    Route::resource('user', 'UserController');

    Route::resource('stroke', 'StrokeController');

    Route::get('/distance', function () {
        return [25, 50, 100, 200, 400, 800];
    });
});
