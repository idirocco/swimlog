import React from "react";
import { render } from "react-dom";
import axios from "axios";
import Timer from "./Timer";
import Chrono from "./Chrono/Chrono";
import {
  MDBContainer,
  MDBTabPane,
  MDBTabContent,
  MDBNav,
  MDBNavItem,
  MDBNavLink,
  MDBCard,
  MDBCardBody,
  MDBTable,
  MDBTableBody,
  MDBTableHead,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBIcon
} from "mdbreact";
import MyContext from './MyContext';

class Heat extends React.Component {
  static contextType = MyContext;

  state = {
    lanes: [],
    running: false,
    error: ""
  };

  handleStartAllClick = () => {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      this._startChrono(lanes[i].chrono, i);
    });
    this.setState({ lanes: lanes, running: true });
  };

  _startChrono = (chrono, i) => {
    chrono.startTime = Date.now();
    chrono.running = true;
    chrono.interval = setInterval(this.tick.bind(this, i), 10);
  };

  handleStopResetClick = event => {
    let lanes = this.state.lanes,
    chrono = lanes[event.target.id].chrono;
    if (chrono.running){
      this.doStop(event.target.id, true);
    } else {
      this.doReset(event.target.id, true);
    }
  };

  handleStartSplitClick = event => {
    let lanes = this.state.lanes,
    chrono = lanes[event.target.id].chrono;
    if (chrono.running){
        chrono.laps.push(
          this._createSplit(chrono)
        );
    } else {
        this._startChrono(chrono, event.target.id);
    }
    this.setState({ lanes: lanes });
  };

  handleResetAllClick = () => {
    this._reset();
  };

  updateLane = index => {
    let lane = this.state.lanes[index],
    currentLaneData = {
        time: "0" + lane.time,
        laps: lane.chrono.laps + "",
        status: "completed"
    };

    axios.put("/api/lane/" + lane.id, currentLaneData)
    .then((result) => {
        this.props.updateLane({
          id: result.data.id,
          time: result.data.time,
          laps: result.data.laps,
          status: result.data.status,
          date: result.data.updated_at
        });
    });
  };

  addSubmitHandler = e => {
    e.preventDefault();
    const user = document.getElementById("user"),
      distance = document.getElementById("distance"),
      stroke = document.getElementById("stroke");

    const params = {
      user_id: user.value,
      stroke_id: stroke.value,
      distance: distance.value
    };

    axios.post("/api/lane", params).then(res => {
      let lanes = this.state.lanes,
        userObj = {
          id: user.options[user.selectedIndex].value,
          name: user.options[user.selectedIndex].text
        },
        strokeObj = {
          id: stroke.options[stroke.selectedIndex].value,
          name: stroke.options[stroke.selectedIndex].text
        };
      lanes.push({
        index: lanes.length,
        id: res.data.id,
        user: userObj,
        distance: distance.options[distance.selectedIndex].value,
        stroke: strokeObj,
        best_time: res.data.best_time,
        chrono: {
          startTime: 0,
          elapsedTime: 0,
          running: false,
          interval: null,
          laps: []
        }
      });
      this.setState({ lanes: lanes });
      this.props.addLane({
        id: res.data.id,
        name: userObj.name,
        stroke: distance.options[distance.selectedIndex].value + ' ' + strokeObj.name,
        time: "00:00.00",
        date: res.data.created_at
      });
    });
  };

  _createSplit = chrono => {
    let tsDiff = chrono.elapsedTime - chrono.startTime;
    chrono.laps.forEach(lapTs => {
      tsDiff -= lapTs;
    });
    return tsDiff;
  };

  doStop = (id, saveState) => {
    let lanes = this.state.lanes;
    lanes[id].chrono.laps.push(this._createSplit(lanes[id].chrono));
    lanes[id].time = this.formatTime(lanes[id].chrono.elapsedTime - lanes[id].chrono.startTime);
    clearInterval(lanes[id].chrono.interval);
    lanes[id].chrono.interval = null;
    lanes[id].chrono.running = false;
    if (typeof saveState !== "undefined" && saveState == true) {
      this.setState({ lanes: lanes });
      this.updateLane(id);
    }
  };

  doReset = (id, saveState) => {
    let lanes = this.state.lanes;
    clearInterval(lanes[id].chrono.interval);
    lanes[id].chrono.laps = [];
    lanes[id].chrono.running = false;
    lanes[id].chrono.interval = null;
    lanes[id].chrono.startTime = 0;
    lanes[id].chrono.elapsedTime = 0;
    if (typeof saveState !== "undefined" && saveState == true) {
      this.setState({ lanes: lanes });
    }
  };

  _stop() {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      this.doStop(lane.id, false);
    });
    this.setState({ lanes: lanes, running: false });
  }

  _reset() {
    let lanes = this.state.lanes;
    lanes.forEach((lane, i) => {
      clearInterval(lane.chrono.interval);
      lane.chrono.laps = [];
      lane.chrono.running = false;
      lane.chrono.interval = null;
      lane.chrono.startTime = 0;
      lane.chrono.elapsedTime = 0;
    });
    this.setState({ lanes: lanes, running: false });
  }

  tick = i => {
    let lanes = this.state.lanes;
    lanes[i].chrono.elapsedTime = Date.now();
    this.setState({ lanes: lanes });
  };


  zeroPad = value => {
    return value < 10 ? `0${value}` : value;
  };

  formatTime = ms => {
    let millis = parseInt((ms % 1000) / 10),
      seconds = parseInt((ms / 1000) % 60),
      minutes = parseInt((ms / (1000 * 60)) % 60);
    minutes = this.zeroPad(minutes);
    seconds = this.zeroPad(seconds);
    millis = this.zeroPad(millis);

    return "0:" + minutes + ":" + seconds + "." + millis;
  };

  render() {
    var { lanes } = this.state;
    let { users, strokes, distances } = this.props;
    let emptyResults =
      lanes.length == 0 ? (
        <tr>
          <td colSpan="5" align="center">
            No results
          </td>
        </tr>
      ) : null;
    return (
      <MDBContainer fluid>
        <MDBCard>
          <MDBCardBody>
            <form onSubmit={this.addSubmitHandler}>
              <MDBRow>
                <MDBCol>
                  <select
                    className="browser-default custom-select"
                    id="user"
                    required
                  >
                    <option value="">User</option>
                    {users.map(user => {
                      return (
                        <option key={user.id} value={user.id}>
                          {user.name}
                        </option>
                      );
                    })}
                  </select>
                </MDBCol>
                <MDBCol>
                  <select
                    className="browser-default custom-select"
                    id="distance"
                    required
                  >
                    <option value="">Distance</option>
                    {distances.map(distance => {
                      return (
                        <option key={distance} value={distance}>
                          {distance}m
                        </option>
                      );
                    })}
                  </select>
                </MDBCol>
                <MDBCol>
                  <select
                    className="browser-default custom-select"
                    id="stroke"
                    required
                  >
                    <option value="">Stroke</option>
                    {strokes.map(stroke => {
                      return (
                        <option key={stroke.id} value={stroke.id}>
                          {stroke.name} ({stroke.abbr})
                        </option>
                      );
                    })}
                  </select>
                </MDBCol>
                <MDBCol>
                  <MDBBtn size="sm" color="success" type="submit">
                    <MDBIcon icon="plus" />
                  </MDBBtn>
                </MDBCol>
              </MDBRow>
            </form>
            <hr />
            <MDBTable>
              <MDBTableHead>
                <tr>
                  <th />
                  <th>Name</th>
                  <th>Distance/Stroke</th>
                  <th>Best Time</th>
                  <th>Current Time</th>
                  <th align="text-right">
                    <MDBBtn
                      outline
                      color="success"
                      size="sm"
                      onClick={this.handleStartAllClick}
                    >
                      <MDBIcon icon="play" /> Start All
                    </MDBBtn>
                    <MDBBtn
                      color="warning"
                      size="sm"
                      onClick={this.handleResetAllClick}
                    >
                      <MDBIcon icon="backspace" /> Reset All
                    </MDBBtn>
                  </th>
                </tr>
              </MDBTableHead>
              <MDBTableBody>
                {emptyResults}
                {lanes.map((lane, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        <MDBIcon icon="arrows-alt-v" />
                      </td>
                      <td>{lane.user.name}</td>
                      <td>
                        {lane.distance}m {lane.stroke.name}
                      </td>
                      <td>{lane.best_time}</td>
                      <td colSpan="2">
                        <Chrono
                          key={index}
                          id={index}
                          startTime={lane.chrono.startTime}
                          elapsedTime={lane.chrono.elapsedTime}
                          running={lane.chrono.running}
                          laps={lane.chrono.laps}
                          _handleStopResetClick={this.handleStopResetClick}
                          _handleStartSplitClick={this.handleStartSplitClick}
                        />
                      </td>
                    </tr>
                  );
                })}
              </MDBTableBody>
            </MDBTable>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default Heat;
