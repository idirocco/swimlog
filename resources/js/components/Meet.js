import React from "react";
import { navigate } from "@reach/router";
import Spinner from "./Spinner";
import Sessions from "./Sessions";
import { Container, Breadcrumb } from "react-bootstrap";
import moment from "moment";

class Meet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      sessions: []
    };
  }

  componentDidMount() {
    fetch("/api/meet/" + this.props.id)
      .then(data => data.json())
      .then(data => {
        let meet;
        meet = data;
        if (Array.isArray(data)) {
          meet = data[0];
        }

        this.setState({
          name: meet.name,
          dateFrom: meet.dateFrom,
          dateTo: meet.dateTo,
          location: meet.location,
          sessions: meet.sessions,
          loading: false
        });
      })
      .catch(err => {
        this.setState({ error: err });
        // navigate("/");
      });
  }

  render() {
    if (this.state.loading) {
      return <Spinner />;
    }

    const { id, name, dateFrom, dateTo, location, sessions } = this.state;

    return (
      <Container fluid="true">
        <Breadcrumb>
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="/meets">Meets</Breadcrumb.Item>
          <Breadcrumb.Item active>{name}</Breadcrumb.Item>
        </Breadcrumb>
        <h1>{name}</h1>
        <p>
          {moment(dateFrom).format("dddd MMMM Do HH:mm")} to{" "}
          {moment(dateTo).format("dddd MMMM Do HH:mm")} at {location}
        </p>
        <Sessions sessions={sessions} />
      </Container>
    );
  }
}

export default Meet;
