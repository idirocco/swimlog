import React from "react";
import { MDBBtn, MDBIcon } from "mdbreact";

class Timer extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    startTS: null,
    diff: null,
    suspended: 0,
    interval: null,
    splits: [],
    splitTS: null,
    splitDiff: null
  };

  getInitialState = () => {
    return {
      startTS: null,
      diff: null,
      suspended: 0,
      interval: null,
      splits: [],
      splitTS: null,
      splitDiff: null
    };
  };

  componentDidMount() {
    // console.log(this.state.startTS);
    this.setState({
      startTS: this.props.startTS,
      diff: this.props.diff
    });
  }

  handleStartClick = () => {
    if (this.state.startTS) {
      // prevent multi clicks on start
      return;
    }
    this.setState({
      startTS: +new Date() - this.state.suspended,
      splitTS: +new Date() - this.state.suspended,
      interval: requestAnimationFrame(this.tick),
      suspended: 0
    });
  };

  handleStopClick = () => {
    cancelAnimationFrame(this.state.interval);
    if (this.state.splitTS) {
      this.addCurrentSplit();
      this.setState({
        startTS: null,
        splitTS: null,
        suspended: +this.state.diff
      });
    }
  };

  handleSplitClick = () => {
    this.addCurrentSplit();
  };

  handleResetClick = () => {
    cancelAnimationFrame(this.state.interval);
    this.setState(this.getInitialState());
  };

  addCurrentSplit = () => {
    let splits = this.state.splits;
    splits.push(this.state.splitDiff);
    this.setState({
      splitTS: +new Date() - this.state.suspended,
      splits: splits
    });
  };

  tick = () => {
    this.setState({
      diff: new Date(+new Date() - this.state.startTS),
      splitDiff: new Date(+new Date() - this.state.splitTS),
      interval: requestAnimationFrame(this.tick)
    });
  };

  addZero = n => {
    return n < 10 ? "0" + n : n;
  };

  render() {
    var diff = this.state.diff;
    var hundredths = diff
      ? Math.round(this.state.diff.getMilliseconds() / 10)
      : 0;
    var seconds = diff ? this.state.diff.getSeconds() : 0;
    var minutes = diff ? this.state.diff.getMinutes() : 0;

    if (hundredths === 100) hundredths = 0;

    return (
      <section className="Chrono">
        <h1>
          {this.addZero(minutes)}:{this.addZero(seconds)}:
          {this.addZero(hundredths)}
        </h1>
        {this.state.splits.map((split, index) => {
          var splhundredths = split
            ? Math.round(split.getMilliseconds() / 10)
            : 0;
          var splseconds = split ? split.getSeconds() : 0;
          var splminutes = split ? split.getMinutes() : 0;

          if (splhundredths === 100) splhundredths = 0;
          return (
            <h4 key={index}>
              Split {index + 1}: {this.addZero(splminutes)}:
              {this.addZero(splseconds)}:{this.addZero(splhundredths)}
            </h4>
          );
        })}
        <div className="buttons">
          <MDBBtn color="success" size="sm" onClick={this.handleStartClick}>
            <MDBIcon icon="clock" className="mr-1" /> Start
          </MDBBtn>
          <MDBBtn color="success" size="sm" onClick={this.handleSplitClick}>
            <MDBIcon icon="clock" className="mr-1" /> Split
          </MDBBtn>
          <MDBBtn color="success" size="sm" onClick={this.handleStopClick}>
            <MDBIcon icon="clock" className="mr-1" /> Stop
          </MDBBtn>
          <MDBBtn color="success" size="sm" onClick={this.handleResetClick}>
            <MDBIcon icon="clock" className="mr-1" /> Reset
          </MDBBtn>
        </div>
      </section>
    );
  }
}

export default Timer;
