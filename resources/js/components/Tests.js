import React from "react";
import Spinner from "./Spinner";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import Heat from "./Heat";
import { Container, Tabs, Tab } from "react-bootstrap";
import MyContext from './MyContext';

var strokeFormatter = (cell, row) => {
  let strokes = cell.split("(");
  return (
    <div>
      {strokes[0]} <span className="d-none d-lg-inline">({strokes[1]}</span>
    </div>
  );
};

const dateFormatter = (cell, row) => {
  let dateObj = cell;
  if (typeof cell !== "object") {
    dateObj = new Date(cell);
  }
  return `${("0" + dateObj.getUTCDate()).slice(-2)}/${(
    "0" +
    (dateObj.getUTCMonth() + 1)
  ).slice(-2)}/${dateObj.getUTCFullYear()}`;
};

const timeFormatter = (cell, row) => {
    return cell ? cell.substring(cell.indexOf(':')+1) : null;
};

const columns = [
  {
    dataField: "id",
    text: "ID",
    classes: "d-none",
    headerClasses: "d-none"
  },
  {
    text: "Name",
    dataField: "name",
    sort: true,
    filter: textFilter()
  },
  {
    text: "Distance/Stroke",
    dataField: "stroke",
    sort: true,
    filter: textFilter(),
    formatter: strokeFormatter
  },
  {
    text: "Time",
    dataField: "time",
    sort: true,
    formatter: timeFormatter
  },
  {
    text: "Date",
    dataField: "date",
    sort: true,
    classes: "d-none d-lg-table-cell",
    headerClasses: "d-none d-lg-table-cell",
    formatter: dateFormatter
  }
];

const defaultSorted = [
  {
    dataField: "date",
    order: "desc"
  }
];

const defaultPaginationOptions = {
  sizePerPage: 20
};

class Tests extends React.Component {
  render() {
      return (
        <MyContext.Consumer>
          {context => (
        <Container fluid="true">
          <Tabs defaultActiveKey="tests" id="uncontrolled-tab-example">
            <Tab eventKey="tests" title="Tests">
              <BootstrapTable
                bootstrap4
                keyField="id"
                data={context.lanes}
                columns={columns}
                defaultSorted={defaultSorted}
                hover
                condensed
                bordered={false}
                pagination={paginationFactory(defaultPaginationOptions)}
                filter={filterFactory()}
              />
            </Tab>
            <Tab eventKey="new" title="New">
              <Heat users={context.users} strokes={context.strokes} distances={context.distances} addLane={context.addLane} updateLane={context.updateLane} />
            </Tab>
          </Tabs>
        </Container>
      )}
      </MyContext.Consumer>
      );
  }
}

export default Tests;
