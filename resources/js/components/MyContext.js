import React from 'react';

const MyContext = React.createContext(
  {
      lanes: [],
      users: [],
      distances: [],
      strokes: [],
      addLane: () => {}
  }
);

export default MyContext;
