import React from "react";
import moment from "moment";
import HeatRow from "./HeatRow";

class Event extends React.Component {
  render() {
    const { id, name, date, heats } = this.props;

    return (
      <div>
        <h4>
          #{id} - {name} - ({moment(date).format("HH:mm")} hs)
        </h4>
        {heats.map(heat => {
          return <HeatRow key={heat.id} heat={heat} />;
        })}
      </div>
    );
  }
}

export default Event;
