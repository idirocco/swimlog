import React from "react";
import MyContext from './MyContext';

class MyProvider extends React.Component {
    state = {
        lanes: [],
        users: [],
        distances: [],
        strokes: [],
    };

    componentDidMount() {
      this.loadLanes();
      this.loadUsers();
      this.loadStrokes();
      this.loadDistances();
    }

    addLane = (lane) => {
      let lanes = this.state.lanes;
      lanes.push(lane);
      this.setState({
        lanes: lanes
      });
    }

    updateLane = (laneObj) => {
      let lanes = this.state.lanes, updatedLane = null;
      lanes.forEach((lane, i) => {
          console.log(lanes.length, i);
        if (lane.id == laneObj.id) {
            updatedLane = lanes.splice(i, 1);
            updatedLane.time = laneObj.time;
            console.log('iiii ', i);
            return;
            // lanes[i].status = laneObj.status;
            // lanes[i].laps = laneObj.laps;
        }
      });
      console.log(updatedLane);
      lanes.push(updatedLane);
      console.log(lanes.length);
      this.setState({
        lanes: lanes
      });

      //for any reason still it does not update the list
    }

    loadUsers = () => {
      fetch("/api/user")
        .then(data => data.json())
        .then(
          data => {
            let users;

            if (data) {
              if (Array.isArray(data)) {
                users = data;
              } else {
                users = [data];
              }
            } else {
              users = [];
            }

            this.setState({
              users: users
            });
          },
          error => {
            this.setState({
              error
            });
          }
        );
    }
    loadStrokes = () => {
      fetch("/api/stroke")
        .then(data => data.json())
        .then(
          data => {
            let strokes;

            if (data) {
              if (Array.isArray(data)) {
                strokes = data;
              } else {
                strokes = [data];
              }
            } else {
              strokes = [];
            }

            this.setState({
              strokes: strokes
            });
          },
          error => {
            this.setState({
              error
            });
          }
        );
    }
    loadDistances = () => {
      fetch("/api/distance")
        .then(data => data.json())
        .then(
          data => {
            let distances;

            if (data) {
              if (Array.isArray(data)) {
                distances = data;
              } else {
                distances = [data];
              }
            } else {
              distances = [];
            }

            this.setState({
              distances: distances
            });
          },
          error => {
            this.setState({
              error
            });
          }
        );

    }
    loadLanes = () => {
      let rows = [];
      fetch("/api/lane")
        .then(data => data.json())
        .then(data => {
            let runs;

            if (data) {
              if (Array.isArray(data)) {
                runs = data;
              } else {
                runs = [data];
              }
            } else {
              runs = [];
            }

            runs.forEach(run => {
              let row = {
                id: run.id,
                name: run.user_name,
                stroke:
                  run.distance +
                  "m " +
                  run.stroke.name +
                  "(" +
                  run.stroke.abbr +
                  ")",
                time: run.time,
                date: run.date.date
              };

              rows.push(row);
            });

            this.setState({
              lanes: rows,
            });
          });
    }

    render() {
        return (
            <MyContext.Provider
                value={{
                    lanes: this.state.lanes,
                    users: this.state.users,
                    distances: this.state.distances,
                    strokes: this.state.strokes,
                    addLane: this.addLane,
                    updateLane: this.updateLane,
                }}
            >
                {this.props.children}
            </MyContext.Provider>
        );
    }
}

export default MyProvider;
