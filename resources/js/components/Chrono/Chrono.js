import React from "react";
import "./Chrono.css";

class Chrono extends React.Component {
  zeroPad(value) {
    return value < 10 ? `0${value}` : value;
  }

  format(ms) {
    let millis = parseInt((ms % 1000) / 10),
      seconds = parseInt((ms / 1000) % 60),
      minutes = parseInt((ms / (1000 * 60)) % 60);
    minutes = this.zeroPad(minutes);
    seconds = this.zeroPad(seconds);
    millis = this.zeroPad(millis);

    return minutes + ":" + seconds + "." + millis;
  }

  render() {
    let { running, laps, startTime, elapsedTime } = this.props;
    let run = running !== false;
    return (
      <main className="chrono">
        <div className="display">
          <div className="time main">
            <div>{this.format(elapsedTime - startTime)}</div>
          </div>
          <div className="laps">
            {laps.map((lap, i) => {
              return (
                <div className="time lap" key={i}>
                  Lap {i + 1}: {this.format(lap)}
                </div>
              );
            })}
          </div>
          <div />
        </div>

        <div className="actions">
        { (running || startTime==0) &&
          <button
            className={
              "btn btn-success btn-sm"
            }
            onClick={this.props._handleStartSplitClick}
            id={this.props.id}
          >
            { running ? 'Split' : 'Start' }
          </button>
        }
          { startTime!=0 &&
          <button
            className={
              "btn btn-warning btn-sm"
            }
            onClick={this.props._handleStopResetClick}
            id={this.props.id}
          >
            { running ? 'Stop' : 'Reset' }
          </button>
            }
        </div>
      </main>
    );
  }
}

export default Chrono;
