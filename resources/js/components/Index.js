import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";

import React from "react";
import ReactDOM from "react-dom";
import { Router } from "@reach/router";
import Meets from "./Meets";
import Meet from "./Meet";
import Tests from "./Tests";
import MyProvider from "./MyProvider";

import { Container, Navbar, Nav, NavDropdown } from "react-bootstrap";
import logo from "./images/logo.png";

class Index extends React.Component {
  render() {
    return (
      <div>
        <Navbar bg="warning" variant="dark">
          <Container fluid="true">
            <Navbar.Brand href="/tests">
              <img
                alt="SwimLog"
                src={logo}
                height="30"
                className="d-inline-block align-top"
              />
              {" SwimLog"}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse
              id="basic-navbar-nav"
              className="justify-content-end"
            >
            <Nav className="mr-auto">
                  <Nav.Link href="/">Tests</Nav.Link>
                  <Nav.Link href="/meets">Meets</Nav.Link>
                </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <div className="pt-3">
          <MyProvider>
          <Router>
            <Tests path="/" />
            <Meets path="/meets" />
            <Meet path="/meet/:id" />
          </Router>
          </MyProvider>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Index />, document.getElementById("root"));
