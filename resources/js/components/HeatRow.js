import React from "react";
import {
  MDBContainer,
  MDBCollapse,
  MDBCard,
  MDBCardBody,
  MDBCollapseHeader
} from "mdbreact";

class HeatRow extends React.Component {
  state = {
    collapsed: true
  };

  toggleCollapse = e => {
    e.preventDefault();
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    const heat = this.props.heat;

    return (
      <div>
        <a href="" onClick={this.toggleCollapse}>
          {heat.name}
        </a>
        <MDBCollapse id="basicCollapse" isOpen={!this.state.collapsed}>
          <ul>
            {heat.lanes.map(lane => {
              return (
                <li>
                  {lane.id} - {lane.user_name} - {lane.time} ({lane.status})
                </li>
              );
            })}
          </ul>
        </MDBCollapse>
      </div>
    );
  }
}

export default HeatRow;
