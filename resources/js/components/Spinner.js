import React from "react";

class Spinner extends React.Component {
  render() {
    const { color = "primary" } = this.props;
    return (
      <div className={"spinner-border text-" + color} role="status">
        <span className="sr-only">Loading...</span>
      </div>
    );
  }
}

export default Spinner;
