import React from "react";
import Spinner from "./Spinner";
import Event from "./Event";
import moment from "moment";

class Sessions extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="sessions">
        {this.props.sessions.map(session => {
          const events = session.events;
          return (
            <div key={session.id}>
              <h2>
                {session.name}{" "}
                <small className="text-muted">
                  ({moment(session.starts_at).format("dddd MMMM Do HH:mm")}hs)
                </small>
              </h2>
              {events.map(event => {
                return (
                  <Event
                    key={event.id}
                    id={event.id}
                    name={event.name}
                    date={event.date}
                    heats={event.heats}
                  />
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

export default Sessions;
